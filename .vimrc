
"Changes are inbuilt into all the files and are permanent when you open a new file

"Map <leader> key to comma
"let mapleader = ','

"Tab width
:set tabstop=4
:set shiftwidth=4

"Show the command at bottom right
set showcmd

"Highlight the search
set hlsearch
set incsearch

"Line Numbering
set nu
set relativenumber

"Auto Indent
set autoindent

"Window Splitting
set splitbelow
set splitright

"Mouse Support
set mouse=a

"####################################################
"#     ABBREVIATIONS AND AUTO CORRECT               #
"####################################################

abbr ll long long
abbr itn int
abbr teh the
abbr my_gmail kartikeya.0005@gmail.com
abbr my_imail kartikey18a@iitg.ac.in
abbr my_name Kartikeya Saxena
abbr my_mobile 8929538642

"Folding text
augroup remember_folds
	autocmd!
	autocmd BufWinLeave * mkview
	autocmd BufWinEnter . silent! loadview
augroup END

nmap <F5> gh
let ghregex = '\(^\|\s\s\)\zs\.\S\+'
let g:netrw_list_hide=ghregex

"Folder view
nmap <F6> I
let g:netrw_banner=0
let g:netrw_liststyle= 3
let g:netrw_winsize= 0
let g:netrw_preview= 1

"plugin code
call plug#begin('~/.vim/plugged')
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'Raimondi/delimitMate'
Plug 'scrooloose/syntastic'
Plug 'scrooloose/nerdtree'
Plug 'rafi/awesome-vim-colorschemes'
Plug 'airblade/vim-gitgutter'
Plug 'tpope/vim-fugitive'
Plug 'haskell/haskell-mode'
Plug 'dense-analysis/ale'
Plug 'mattn/emmet-vim'


call plug#end()

set nocompatible
filetype off

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
Plugin 'VundleVim/Vundle.vim'
Plugin 'pangloss/vim-javascript'
Plugin 'mxw/vim-jsx'
Plugin 'othree/yajs.vim'
"Plugin 'eagletmt/ghcmod-vim'
"Plugin 'Shougo/vimproc'
call vundle#end()
filetype plugin indent on

" FORMATTERS
au FileType javascript setlocal formatprg=prettier
au FileType javascript.jsx setlocal formatprg=prettier
au FileType typescript setlocal formatprg=prettier\ --parser\ typescript
au FileType html setlocal formatprg=js-beautify\ --type\ html
au FileType scss setlocal formatprg=prettier\ --parser\ css
au FileType css setlocal formatprg=prettier\ --parser\ css


" Start NERDTree and put the cursor back in the other window.
autocmd VimEnter * NERDTree | wincmd p


" Exit Vim if NERDTree is the only window left.
autocmd BufEnter * if tabpagenr('$') == 1 && winnr('$') == 1 && exists('b:NERDTree') && b:NERDTree.isTabTree() |
    \ quit | endif

" Start NERDTree on every Tab
autocmd BufWinEnter * NERDTreeMirror

" Color theme
colorscheme OceanicNext

let g:airline#extensions#tabline#enabled = 1

" Linting
let g:ale_linters = {
\   'python': ['flake8', 'pylint'],
\   'javascript': ['eslint'],
\   'vue': ['eslint'],
\   'haskell': ['hlint', 'hdevtools', 'hfmt'],
\}

let g:ale_fixers = {
  \    'javascript': ['eslint'],
  \    'typescript': ['prettier', 'tslint'],
  \    'vue': ['eslint'],
  \    'scss': ['prettier'],
  \    'html': ['prettier'],
  \    'reason': ['refmt']
\}
let g:ale_fix_on_save = 1
let g:ale_sign_error = '●'
let g:ale_sign_warning = '!'

function! LinterStatus() abort
    let l:counts = ale#statusline#Count(bufnr(''))
    let l:all_errors = l:counts.error + l:counts.style_error
    let l:all_non_errors = l:counts.total - l:all_errors
    return l:counts.total == 0 ? 'OK' : printf(
        \   '%d⨉ %d⚠ ',
        \   all_non_errors,
        \   all_errors
        \)
endfunction
set statusline+=%=
set statusline+=\ %{LinterStatus()}

nmap <silent> <C-e> <Plug>(ale_next_wrap)


" Track the engine.
Plugin 'SirVer/ultisnips'

" Snippets are separated from the engine. Add this if you want them:
Plugin 'honza/vim-snippets'

" Trigger configuration. You need to change this to something other than <tab> if you use one of the following:
" - https://github.com/Valloric/YouCompleteMe
" - https://github.com/nvim-lua/completion-nvim
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<c-b>"
let g:UltiSnipsJumpBackwardTrigger="<c-z>"

" If you want :UltiSnipsEdit to split your window.
let g:UltiSnipsEditSplit="vertical"
